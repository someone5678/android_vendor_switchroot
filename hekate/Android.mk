LOCAL_PATH := $(call my-dir)

# Hekate Android payload config
include $(CLEAR_VARS)
LOCAL_MODULE        := 00-android
LOCAL_MODULE_SUFFIX := .ini
LOCAL_SRC_FILES     := 00-android.ini
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)
include $(BUILD_PREBUILT)
INSTALLED_RADIOIMAGE_TARGET += $(PRODUCT_OUT)/00-android.ini

# Hekate logo images
include $(CLEAR_VARS)
LOCAL_MODULE        := bootlogo_android
LOCAL_MODULE_SUFFIX := .bmp
LOCAL_SRC_FILES     := bootlogo_android.bmp
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)
include $(BUILD_PREBUILT)
INSTALLED_RADIOIMAGE_TARGET += $(PRODUCT_OUT)/bootlogo_android.bmp

include $(CLEAR_VARS)
LOCAL_MODULE        := icon_android_hue
LOCAL_MODULE_SUFFIX := .bmp
LOCAL_SRC_FILES     := icon_android_hue.bmp
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)
include $(BUILD_PREBUILT)
INSTALLED_RADIOIMAGE_TARGET += $(PRODUCT_OUT)/icon_android_hue.bmp
